FROM tiangolo/uvicorn-gunicorn:python3.8
# Establish the runtime user (with no password and no sudo)
RUN adduser  --disabled-login app
USER app
# set the working directory in the container to be the /app
WORKDIR app
# expose the port that uvicorn will run the app
EXPOSE 8000
# copy the local requirements.txt file to the /app/requirements.txt in the container
COPY --chown=app:app requirements.txt /app/requirements.txt
# install the packages from the requirements.txt file in the container
RUN pip install --user --no-cache-dir -r /app/requirements.txt
ENV PATH="/home/app/.local/bin:${PATH}"
# copy the local files to the /app folder in the container
COPY --chown=app:app  ./app /app

