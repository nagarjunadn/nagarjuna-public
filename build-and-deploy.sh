#!/bin/bash

#Build Docker image
echo "Building docker images for python app"
docker build -t nagasearch-image:v1 .
echo "Building docker images for python app completed"
#Run docker container locally to test
#echo "Starting container locally to test"
#docker run -d --name nagasearch-container -p 80:80 nagasearch-image:v1
#To test conteiner app works locally
#echo "Started container and listening"
#sleep 10s
#curl localhost/tree
#sleep 10s

#Set into minikube env to upload docker image to minikube to load and use during pod deployment
#minikube start #assuming minikube is up and running
eval $(minikube docker-env)
docker build -t nagasearch-image:v1  .

#Enable Ingress for minikube
#minikube addons enable ingress #Assuming addon for ingress enabled

#Deploy pod,service and ingress
kubectl apply -f deployment.yaml

#get service end point of nodeport and make sure service end point works
#minikube service list
#echo "Application result of service end point"
#sleep 10s
#curl $(minikube ip):30163/tree

#Get minikube ip for ingress to add host entry for domain
echo "end point of minikube:"
minikube ip
minikube_ip=$(minikube ip)
#Add localidns lookup
echo "Adding local dns lookup"
echo $minikube_ip local.ecoasia.org | sudo tee -a /etc/hosts

#To access app
echo "Waiting for 10sec for application to come up on minikube"
sleep 10s
echo "Application result of URL:local.ecoasia.org/tree"
curl local.ecoasia.org/tree
