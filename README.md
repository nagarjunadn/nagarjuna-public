
#Used EC2 Amazon Linux 2 instance to develop app and deploy:
- python and pip are coming inbuilt so no need to install

#Using FastAPI to develop python web app hence required packages must be installed
- pip3 install fastapi
- pip3 install "uvicorn[standard]"

#Run python code locally

 cd app

 sudo uvicorn main:app --reload --host 0.0.0.0 --port 8000

#Access Locally or using public IP

curl localhost:8000/tree

curl EC2PUBLICIP:8000/tree

#Install and configure Docker, refer https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html

sudo amazon-linux-extras install docker -y

sudo yum install docker

docker --version

sudo service docker start

sudo systemctl enable docker

sudo usermod -a -G docker ec2-user

docker info #run this reconnectiong session to avoid following error
Error: ERROR: Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get "http://%2Fvar%2Frun%2Fdocker.sock/v1.24/info": dial unix /var/run/docker.sock: connect: permission denied

#Build Docker image

docker build -t nagasearch-image .

#Run docker container locally to test

docker run -d --name nagasearch-icontainer -p 80:80 nagasearch-image


#Minikube Instalilation

curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && chmod +x minikube

#To add the Minikube executable to your path:

sudo cp minikube /usr/local/bin && rm minikube

sudo minikube start

#Install kubectl

curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

#To add the kubectl executable to your path:

chmod +x kubectl && sudo cp kubectl /usr/local/bin && rm kubectl

#Set into minikube env to upload docker image to minikube to load and use during pod deployment

eval $(minikube docker-env)

docker build -t nagasearch .

#Enable Ingress for minikube

minikube addons enable ingress

#Deploy pod,service and ingress

kubectl apply -f deployment.yaml

#get service end point of nodeport

minikube service list

#Get minikube ip to add host entry for domain

minikube ip

minikube_ip=$(minikube ip)

echo $minikube_ip local.example.org >>/etc/hosts

#To access app

curl local.example.org/tree

Will return output:
{"myFavouriteTree":"Mango"}
